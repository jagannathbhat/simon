# frozen_string_literal: true

class ExpensePolicy
  attr_reader :user, :expense

  def initialize(user, expense)
    @user = user
    @expense = expense
  end

  def create?
    expense.user_id == user.id
  end

  def destroy?
    create?
  end

  def show?
    create?
  end

  def update?
    create?
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.where(user_id: user.id).order(created_at: :desc)
    end
  end
end
