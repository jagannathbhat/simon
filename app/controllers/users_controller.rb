# frozen_string_literal: true

class UsersController < ApplicationController
  # before_action :load_user, only: %i[ show edit update destroy ]

  # GET /users/1 or /users/1.json
  # def show
  # end

  # GET /users/1/edit
  # def edit
  # end

  def create
    @user = User.new(user_params)
    if @user.save
      render status: :ok, json: { notice: t("successfuly_created", entity: "user") }
    else
      errors = @user.errors.full_messages.to_sentence
      render status: :unprocessable_entity, json: { errors: errors }
    end
  end

  # def update
  #   respond_to do |format|
  #     if @user.update(user_params)
  #       format.html { redirect_to @user, notice: "User was successfully updated." }
  #       format.json { render :show, status: :ok, location: @user }
  #     else
  #       format.html { render :edit, status: :unprocessable_entity }
  #       format.json { render json: @user.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # def destroy
  #   @user.destroy
  #   respond_to do |format|
  #     format.html { redirect_to users_url, notice: "User was successfully destroyed." }
  #     format.json { head :no_content }
  #   end
  # end

  private

    # Use callbacks to share common setup or constraints between actions.
    # def load_user
    #   @user = User.find_by_id!(params[:id])
    # rescue ActiveRecord::RecordNotFound => errors
    #   render json: { errors: errors }
    # end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:username, :password, :password_confirmation)
    end
end
