# frozen_string_literal: true

class SessionsController < ApplicationController
  before_action :authenticate_user_using_x_auth_token, only: [:destroy]

  def create
    @user = User.find_by(username: login_params[:username].downcase)
    unless @user.present? && @user.authenticate(login_params[:password])
      render status: :unauthorized, json: { error: t("session.incorrect_credentials") }
    end
  end

  def logout
    @current_user = nil
  end

  private

    def login_params
      params.require(:user).permit(:username, :password)
    end
end
