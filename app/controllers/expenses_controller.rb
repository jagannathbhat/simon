# frozen_string_literal: true

class ExpensesController < ApplicationController
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: :index

  before_action :authenticate_user_using_x_auth_token
  before_action :load_expense, except: :index

  def create
    @expense = Expense.new(expense_params)
    @expense.user_id = @current_user.id
    if @expense.save
      render status: :ok, json: { notice: t("successfuly_created", entity: "expense"), expense: @expense }
    else
      errors = @expense.errors.full_messages.to_sentence
      render status: :unprocessable_entity, json: { errors: errors }
    end
  end

  def destroy
    if @expense.destroy
      render status: :ok, json: { notice: t("successfuly_deleted", entity: "expense") }
    else
      errors = @expense.errors.full_messages.to_sentence
      render status: :unprocessable_entity, json: { errors: errors }
    end
  end

  def index
    @expenses = policy_scope(Expense)
    render status: :ok, json: @expenses
  end

  def show
    render
  end

  def update
    if @expense.update(expense_params)
      render status: :ok, json: { notice: t("successfuly_updated", entity: "expense"), expense: @expense }
    else
      render status: :unprocessable_entity, json: { errors: @expense.errors.full_messages.to_sentence }
    end
  end

  private

    def load_expense
      @expense = Expense.find_by_id!(params[:id])
      authorize @expense
    rescue ActiveRecord::RecordNotFound => errors
      render json: { errors: errors }
    end

    def expense_params
      params.require(:expense).permit(:amount, :name)
    end
end
