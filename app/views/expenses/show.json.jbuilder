# frozen_string_literal: true

json.task do
  json.extract! @expense,
    :id,
    :name,
    :amount,
    :created_at,
    :user_id
end
