# frozen_string_literal: true

class User < ApplicationRecord
  has_many :expenses, dependent: :destroy
  has_secure_password
  has_secure_token :authentication_token

  validates :username, presence: true, length: { maximum: 50 }
  validates :password, presence: true, length: { minimum: 6, maximum: 50 }, if: -> { password.present? }
  validates :password_confirmation, presence: true, length: { maximum: 50 }, on: :create
end
