# frozen_string_literal: true

class Expense < ApplicationRecord
  belongs_to :user

  validates :amount, presence: true, numericality: { only_float: true }
  validates :name, presence: true, length: { maximum: 50 }
end
