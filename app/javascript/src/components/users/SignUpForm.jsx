import React from "react";

import PropTypes from "prop-types";

import Button from "components/common/Button";
import Input from "components/common/Input";

const SignUpForm = ({
  setPassword,
  setPasswordConfirmation,
  setUsername,
  values = {}
}) => {
  return (
    <>
      <Input
        autoFocus={true}
        onChange={e => setUsername(e.target.value)}
        placeholder="Username"
        wrapperProps={{ className: "flex-auto mt-4" }}
        value={values.username}
      />
      <Input
        onChange={e => setPassword(e.target.value)}
        placeholder="Password"
        type="password"
        wrapperProps={{ className: "flex-1 mt-4" }}
        value={values.password}
      />
      <Input
        onChange={e => setPasswordConfirmation(e.target.value)}
        placeholder="Confirm Password"
        type="password"
        wrapperProps={{ className: "flex-1 mt-4" }}
        value={values.password_confirmation}
      />
      <div className="mt-4">
        <Button
          className="cursor-pointer"
          component="input"
          type="submit"
          value="Sign Up"
        />
      </div>
    </>
  );
};

SignUpForm.propTypes = {
  setPassword: PropTypes.func,
  setPasswordConfirmation: PropTypes.func,
  setUsername: PropTypes.func,
  values: PropTypes.object
};

export default SignUpForm;
