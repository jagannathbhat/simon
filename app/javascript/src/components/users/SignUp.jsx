import React, { useState } from "react";

import { Link } from "react-router-dom";

import usersApi from "apis/users";

import SignUpForm from "./SignUpForm";

const SignUp = ({ history }) => {
  const [password, setPassword] = useState("");
  const [password_confirmation, setPasswordConfirmation] = useState("");
  const [username, setUsername] = useState("");

  async function submitHandler(e) {
    e.preventDefault();

    try {
      await usersApi.create({
        user: { password, password_confirmation, username }
      });
      history.push("/");
    } catch (error) {
      logger.error(error);
    }
  }

  return (
    <form
      className="mx-auto my-8 text-center max-w-lg"
      onSubmit={submitHandler}
    >
      <h1 className="font-bold text-2xl">Sign Up</h1>
      <Link className="font-bold text-bb-purple" to="/login">
        or Login to existing account
      </Link>
      <SignUpForm
        setPassword={setPassword}
        setPasswordConfirmation={setPasswordConfirmation}
        setUsername={setUsername}
        values={{ password, password_confirmation, username }}
      />
    </form>
  );
};

export default SignUp;
