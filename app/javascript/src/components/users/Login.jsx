import React, { useState } from "react";

import { Link } from "react-router-dom";

import sessionsApi from "apis/sessions";
import { setToLocalStorage } from "helpers/storage";

import LoginForm from "./LoginForm";

const Login = () => {
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");

  async function submitHandler(e) {
    e.preventDefault();

    try {
      const { data } = await sessionsApi.create({
        user: { password, username }
      });
      setToLocalStorage({ authToken: data.authentication_token, username });
      window.location.href = "/";
    } catch (error) {
      logger.error(error);
    }
  }

  return (
    <form
      className="mx-auto my-8 text-center max-w-lg"
      onSubmit={submitHandler}
    >
      <h1 className="font-bold text-2xl">Log In</h1>
      <Link className="font-bold text-bb-purple" to="/signup">
        or Create Account
      </Link>
      <LoginForm
        setPassword={setPassword}
        setUsername={setUsername}
        values={{ password, username }}
      />
    </form>
  );
};

export default Login;
