import React from "react";

import { resetAuthTokens } from "apis/axios";
import sessionsApi from "apis/sessions";
import { getFromLocalStorage, setToLocalStorage } from "helpers/storage";

const NavBar = () => {
  async function handleLogout() {
    try {
      await sessionsApi.logout();
      setToLocalStorage({
        authToken: null,
        username: null
      });
      resetAuthTokens();
      window.location.href = "/";
    } catch (error) {
      logger.error(error);
    }
  }

  return (
    <div className="flex justify-between mb-12 p-4 shadow-md w-full">
      <div>
        <h1 className="font-bold hidden sm:block">Simon</h1>
      </div>
      <div>
        <a className="cursor-pointer font-bold mr-6" href="#">
          {getFromLocalStorage("username")}
        </a>
        <a className="cursor-pointer" onClick={handleLogout}>
          Logout
        </a>
      </div>
    </div>
  );
};

export default NavBar;
