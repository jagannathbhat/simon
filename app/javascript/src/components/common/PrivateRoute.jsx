import React from "react";

import PropTypes from "prop-types";
import { Redirect, Route } from "react-router-dom";

const PrivateRoute = ({
  component: Component,
  condition,
  redirectRoute,
  ...props
}) => {
  if (!condition) {
    return <Redirect to={{ pathname: redirectRoute, from: props.location }} />;
  }
  return <Route component={Component} {...props} />;
};

PrivateRoute.propTypes = {
  component: PropTypes.func,
  condition: PropTypes.bool,
  redirectRoute: PropTypes.string
};

export default PrivateRoute;
