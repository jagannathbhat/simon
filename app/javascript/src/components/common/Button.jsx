import React from "react";

import PropTypes from "prop-types";

const Button = ({
  children,
  className,
  color = "bb-purple",
  component = "button",
  loading,
  onClick,
  ...props
}) => {
  const TagName = component;
  return (
    <TagName
      onClick={onClick}
      className={
        `bg-${color} border border-transparent duration-150 ease-in-out flex font-medium group
        justify-center leading-5 relative rounded-md text-white transition px-4 py-2 text-sm w-full
        hover:bg-opacity-90 focus:outline-none ` + className
      }
      {...props}
    >
      {loading ? "Loading..." : children}
    </TagName>
  );
};

Button.propTypes = {
  children: PropTypes.string,
  className: PropTypes.string,
  color: PropTypes.string,
  component: PropTypes.string,
  loading: PropTypes.bool,
  onClick: PropTypes.func
};
export default Button;
