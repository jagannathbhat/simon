import React from "react";

import PropTypes from "prop-types";

const Input = ({
  className,
  label,
  placeholder,
  required = true,
  type = "text",
  value,
  wrapperProps,
  onChange,
  ...props
}) => (
  <div className={"mt-6 " + wrapperProps.className} {...wrapperProps}>
    {label && (
      <label
        className="block text-sm font-medium
              leading-5 text-bb-gray-700"
      >
        {label}
      </label>
    )}
    <div className="rounded-md shadow-sm">
      <input
        type={type}
        required={required}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        className={`block w-full px-3 py-2 placeholder-gray-400
          transition duration-150 ease-in-out border
          border-gray-300 rounded-md appearance-none
          focus:outline-none focus:shadow-outline-blue
          focus:border-blue-300 sm:text-sm sm:leading-5 ${className}`}
        {...props}
      />
    </div>
  </div>
);

Input.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.node,
  wrapperProps: PropTypes.object,
  onChange: PropTypes.func
};

export default Input;
