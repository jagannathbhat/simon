import React, { useState } from "react";

import PropTypes from "prop-types";

import expensesApi from "apis/expenses";

import ExpenseForm from "./ExpenseForm";

const ExpenseAdd = ({ expenseAdd }) => {
  const [amount, setAmount] = useState("");
  const [name, setName] = useState("");

  async function submitHandler(e) {
    e.preventDefault();

    try {
      const { data } = await expensesApi.create({ expense: { amount, name } });
      expenseAdd(data.expense);
      setAmount("");
      setName("");
    } catch (error) {
      logger.error(error);
    }
  }

  return (
    <form className="flex items-center justify-center" onSubmit={submitHandler}>
      <ExpenseForm
        setAmount={setAmount}
        setName={setName}
        values={{ amount, name }}
      />
    </form>
  );
};

ExpenseAdd.propTypes = { expenseAdd: PropTypes.func };

export default ExpenseAdd;
