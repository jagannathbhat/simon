import React, { useState } from "react";

import PropTypes from "prop-types";

import { formatCurrency } from "utils/currency";
import { formatDate } from "utils/date";

import ExpenseUpdate from "./ExpenseUpdate";

const ExpensesList = ({ expenses, expenseDelete, expenseUpdate }) => {
  const [editExpense, setEditExpense] = useState(null);

  if (expenses.length === 0) {
    return <p className="mt-10 text-gray-500 text-lg">No Expenses to show</p>;
  }

  return (
    <>
      {expenses.map(expense =>
        editExpense === expense.id ? (
          <ExpenseUpdate
            key={expense.id}
            expenseUpdate={expense => {
              setEditExpense(null);
              expenseUpdate(expense);
            }}
            values={expense}
          />
        ) : (
          <div
            key={expense.id}
            className="flex items-center justify-between my-4"
          >
            <p className="mr-2 text-left w-1/2">{expense.name}</p>
            <p className="flex-1 mr-2 text-left">
              Rs. {formatCurrency(expense.amount)}
            </p>
            <p className="flex-1 mr-4 text-right">
              {formatDate(expense.created_at)}
            </p>
            <i
              className="ri-pencil-fill cursor-pointer mr-4"
              onClick={() => setEditExpense(expense.id)}
            ></i>
            <i
              className="ri-delete-bin-2-fill cursor-pointer"
              onClick={() => expenseDelete(expense.id)}
            ></i>
          </div>
        )
      )}
    </>
  );
};

ExpensesList.propTypes = {
  expenses: PropTypes.array,
  expenseDelete: PropTypes.func,
  expenseUpdate: PropTypes.func
};

export default ExpensesList;
