import React, { useState } from "react";

import PropTypes from "prop-types";

import expensesApi from "apis/expenses";

import ExpenseForm from "./ExpenseForm";

const ExpenseUpdate = ({ expenseUpdate, values }) => {
  const [amount, setAmount] = useState(values.amount);
  const [name, setName] = useState(values.name);

  async function submitHandler(e) {
    e.preventDefault();
    if (amount === values.amount && name === values.name)
      return expenseUpdate();

    try {
      const { data } = await expensesApi.update(values.id, {
        expense: { amount, name }
      });
      return expenseUpdate(data.expense);
    } catch (error) {
      logger.error(error);
      return expenseUpdate();
    }
  }

  return (
    <form className="flex items-center justify-center" onSubmit={submitHandler}>
      <ExpenseForm
        setAmount={setAmount}
        setName={setName}
        values={{
          amount,
          name
        }}
      />
    </form>
  );
};

ExpenseUpdate.propTypes = {
  expenseUpdate: PropTypes.func,
  values: PropTypes.object
};

export default ExpenseUpdate;
