import React from "react";

import PropTypes from "prop-types";

import Button from "components/common/Button";
import Input from "components/common/Input";

const ExpenseForm = ({ setAmount, setName, values = {} }) => {
  return (
    <>
      <Input
        onChange={e => setName(e.target.value)}
        placeholder="New Expense"
        wrapperProps={{ className: "flex-auto mr-2" }}
        value={values.name}
      />
      <Input
        onChange={e => setAmount(parseFloat(e.target.value))}
        placeholder="Amount"
        type="number"
        wrapperProps={{ className: "flex-1 mr-2" }}
        value={values.amount}
      />
      <div className="w-12">
        <Button component="input" type="submit" value="Save" />
      </div>
    </>
  );
};

ExpenseForm.propTypes = {
  setAmount: PropTypes.func,
  setName: PropTypes.func,
  values: PropTypes.object
};

export default ExpenseForm;
