import React, { useEffect, useState } from "react";

import expensesApi from "apis/expenses";
import ExpenseAdd from "components/expenses/ExpenseAdd";
import ExpensesList from "components/expenses/ExpensesList";

const HomePage = () => {
  const [expenses, setExpenses] = useState([]);

  function expenseAdd(expense) {
    setExpenses([expense, ...expenses]);
  }

  async function expenseDelete(deletedExpense) {
    try {
      await expensesApi.destroy(deletedExpense);
      setExpenses(expenses.filter(expense => expense.id !== deletedExpense));
    } catch (error) {
      logger.error(error);
    }
  }

  function expenseUpdate(updatedExpense) {
    if (updatedExpense)
      setExpenses(
        expenses.map(expense =>
          expense.id === updatedExpense.id ? updatedExpense : expense
        )
      );
  }

  async function getExpenses() {
    try {
      const { data } = await expensesApi.list();
      if (data.length > 0) setExpenses(data);
    } catch (error) {
      logger.error(error);
    }
  }

  useEffect(() => getExpenses(), []);

  return (
    <div className="m-2 mt-4 text-center">
      <p className="font-bold mb-6 text-xl">Total Expenses</p>
      <div className="max-w-xl mx-auto">
        <ExpenseAdd expenseAdd={expenseAdd} />
      </div>
      <div className="max-w-xl mt-6 mx-auto">
        <ExpensesList
          expenses={expenses}
          expenseDelete={expenseDelete}
          expenseUpdate={expenseUpdate}
        />
      </div>
    </div>
  );
};

export default HomePage;
