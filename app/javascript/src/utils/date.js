const getDayName = day => {
  switch (day) {
    case 0:
      return "Sun";
    case 1:
      return "Mon";
    case 2:
      return "Tue";
    case 3:
      return "Wed";
    case 4:
      return "Thu";
    case 5:
      return "Fri";
    case 6:
      return "Sat";
    default:
      return "";
  }
};

const getMonthName = month => {
  switch (month) {
    case 0:
      return "Jan";
    case 1:
      return "Feb";
    case 2:
      return "Mar";
    case 3:
      return "Apr";
    case 4:
      return "May";
    case 5:
      return "Jun";
    case 6:
      return "Jul";
    case 7:
      return "Aug";
    case 8:
      return "Sep";
    case 9:
      return "Oct";
    case 10:
      return "Nov";
    case 11:
      return "Dec";
    default:
      return "";
  }
};

export const formatDate = input => {
  const current = new Date();
  const date = new Date(input);

  if (date.getFullYear() === current.getFullYear()) {
    if (current.getDate() - date.getDate() < 7)
      return getDayName(date.getDay());
    return `${date.getDate()} ${getMonthName(date.getMonth())}`;
  }

  return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
};
