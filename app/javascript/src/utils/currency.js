export const formatCurrency = amount => {
  const [amountWholeNumbers, amountDecimals] = amount.toString().split(".");

  let wholeReversed = amountWholeNumbers.split("").reverse();
  wholeReversed = wholeReversed.slice(0, 3).concat(
    wholeReversed.slice(3).reduce((num, digit, index) => {
      if (index % 2 === 0) return num.concat([",", digit]);
      return num.concat([digit]);
    }, [])
  );

  return `${wholeReversed.reverse().join("")}.${
    amountDecimals ? amountDecimals : "00"
  }`;
};
