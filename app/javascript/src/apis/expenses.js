import axios from "axios";

const create = payload => axios.post("/expenses/", payload);

const destroy = id => axios.delete(`/expenses/${id}`);

const list = () => axios.get("/expenses");

const update = (id, payload) => axios.put(`/expenses/${id}`, payload);

const expensesApi = {
  create,
  destroy,
  list,
  update
};

export default expensesApi;
