import axios from "axios";

const create = payload => axios.post("/users/", payload);

const expensesApi = { create };

export default expensesApi;
