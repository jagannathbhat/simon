import axios from "axios";

const create = payload => axios.post("/sessions/", payload);

const logout = () => axios.delete("/sessions");

const sessionsApi = { create, logout };

export default sessionsApi;
