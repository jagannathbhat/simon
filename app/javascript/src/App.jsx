import React, { useEffect, useState } from "react";

import { either, isEmpty, isNil } from "ramda";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import { registerIntercepts, setAuthHeaders } from "apis/axios";
import { initializeLogger } from "common/logger";
import NavBar from "components/common/NavBar";
import PrivateRoute from "components/common/PrivateRoute";
import HomePage from "components/HomePage";
import Login from "components/users/Login";
import SignUp from "components/users/SignUp";
import { getFromLocalStorage } from "helpers/storage";

const App = () => {
  const [loading, setLoading] = useState(true);

  const authToken = getFromLocalStorage("authToken");
  const isLoggedIn = !either(isNil, isEmpty)(authToken) && authToken !== "null";

  useEffect(() => {
    initializeLogger();
    registerIntercepts();
    setAuthHeaders(setLoading);
  }, []);

  if (loading) {
    return (
      <div className="flex h-screen items-center justify-center w-full">
        <h1 className="text-2xl">Loading...</h1>
      </div>
    );
  }

  return (
    <Router>
      {isLoggedIn && <NavBar />}
      <Switch>
        <Route component={SignUp} exact path="/signup" />
        <Route component={Login} exact path="/login" />
        <PrivateRoute
          component={HomePage}
          condition={isLoggedIn}
          exact
          path="/"
          redirectRoute="/login"
        />
      </Switch>
      <ToastContainer />
    </Router>
  );
};

export default App;
