const getFromLocalStorage = key => localStorage.getItem(key);

function setToLocalStorage({ authToken, username }) {
  localStorage.setItem("authToken", authToken);
  localStorage.setItem("username", username);
}

export { setToLocalStorage, getFromLocalStorage };
