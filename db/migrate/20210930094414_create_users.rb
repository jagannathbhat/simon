# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :username, null: false, index: { unique: true }, limit: 50
      t.string :password_digest, null: false, limit: 50
      t.string :authentication_token
      t.timestamps
    end
    add_column :expenses, :user_id, :integer
    add_foreign_key :expenses, :users, column: :user_id, on_delete: :cascade
  end
end
