# frozen_string_literal: true

class CreateExpenses < ActiveRecord::Migration[6.1]
  def change
    create_table :expenses do |t|
      t.float :amount, null: false
      t.string :name, null: false, limit: 50
      t.timestamps
    end
  end
end
