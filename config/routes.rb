# frozen_string_literal: true

Rails.application.routes.draw do
  defaults format: :json do
    resources :users, only: :create
    resources :sessions, only: :create
    resources :expenses, only: %i[create destroy index show update], param: :id

    delete "/sessions", to: "sessions#logout"
  end

  root "home#index"
  get "*path", to: "home#index", via: :all
end
